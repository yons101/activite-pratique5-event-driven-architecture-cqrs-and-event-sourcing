package com.example.queries.services;

import com.example.commonapi.events.AccountActivatedEvent;
import com.example.commonapi.events.AccountCreatedEvent;
import com.example.commonapi.events.AccountDebitedEvent;
import com.example.commonapi.queries.GetAccountById;
import com.example.commonapi.queries.GetAllAccountQuery;
import com.example.queries.entities.Account;
import com.example.queries.repositories.AccountRepository;
import com.example.queries.repositories.TransactionRepository;
import com.example.commonapi.enums.TransactionType;
import com.example.commonapi.events.AccountCreditedEvent;
import com.example.queries.entities.AccountTransaction;
import lombok.AllArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Service
public class AccountServiceHandler {

    AccountRepository accountRepository;
    TransactionRepository transactionRepository;

    @EventHandler
    @Transactional
    public void on(AccountCreatedEvent event) {

        Account account = new Account();

        account.setId(event.getId());
        account.setBalance(event.getInitialBalance());
        account.setCurrency(event.getCurrency());
        account.setStatus(event.getStatus());
        accountRepository.save(account);
    }

    @EventHandler
    @Transactional
    public void on(AccountActivatedEvent event) {
        Account account = accountRepository.findById(event.getId()).get();
        account.setStatus(event.getStatus());
        accountRepository.save(account);
    }

    @EventHandler
    @Transactional
    public void on(AccountDebitedEvent event) {
        AccountTransaction accountTransaction = new AccountTransaction();
        Account account = accountRepository.findById(event.getId()).get();
        account.setBalance(account.getBalance() - event.getAmount());

        accountTransaction.setTimestamp(new Date());
        accountTransaction.setType(TransactionType.DEBIT);
        accountTransaction.setAmount(event.getAmount());
        accountTransaction.setAccount(account);
        transactionRepository.save(accountTransaction);
        accountRepository.save(account);
    }

    @EventHandler
    @Transactional
    public void on(AccountCreditedEvent event) {
        AccountTransaction accountTransaction = new AccountTransaction();
        Account account = accountRepository.findById(event.getId()).get();
        account.setBalance(account.getBalance() + event.getAmount());
        accountTransaction.setTimestamp(new Date());
        accountTransaction.setType(TransactionType.CREDIT);
        accountTransaction.setAmount(event.getAmount());
        accountTransaction.setAccount(account);
        transactionRepository.save(accountTransaction);
        accountRepository.save(account);
    }

    @QueryHandler
    public List<Account> on(GetAllAccountQuery getAllAccountQuery) {
        return accountRepository.findAll();
    }

    @QueryHandler
    public Account on(GetAccountById getAccountById) {
        return accountRepository.findById(getAccountById.getId()).get();
    }
}
