package com.example.commonapi.commandes;

import lombok.Getter;

public class CreditAccountCommand extends BaseCommand<String>{
 @Getter
 private final double amount;
 @Getter
 private final String currency;

    public CreditAccountCommand(String id, double amount, String currency) {

        super(id);
        this.amount = amount;
        this.currency = currency;
    }
}
