package com.example.commonapi.enums;

public enum AccountStatus {
    CREATED,DEBITED,SUSPENDED,ACTIVATED
}
