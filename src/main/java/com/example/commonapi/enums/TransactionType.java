package com.example.commonapi.enums;

public enum TransactionType {
    CREDIT, DEBIT
}
